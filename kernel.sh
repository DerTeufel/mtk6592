#!/bin/bash
#Stop script if something is broken
set -e

#Export CROSS_COMPILE to point toolchain
#export CROSS_COMPILE="ccache ../prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.6/bin/arm-linux-androideabi-"
export CROSS_COMPILE="ccache ../../prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.6/bin/arm-linux-androideabi-"
export TARGET_PRODUCT=`cat DEVICE_TREE`
export ARCH_MTK_PLATFORM="mt6592"
#export BUILT_FROM_KERNEL_DIR="1"
export MTK_PATH_PLATFORM="/home/dominik/mtk92/kernel/mediatek/platform/mt6592/kernel"
export MTK_PATH_SOURCE="/home/dominik/mtk92/kernel/mediatek/kernel"
C_INCLUDE_PATH="/home/dominik/mtk92/kernel/mediatek/platform/mt6592/kernel/drivers/dispsys"
C_INCLUDE_PATH=$C_INCLUDE_PATH:"/home/dominik/mtk92/kernel/mediatek/platform/mt6592/kernel/drivers/video":"/home/dominik/mtk92/kernel/mediatek/platform/mt6592/kernel/core/include/mach":"/home/dominik/mtk92/kernel/mediatek/kernel/drivers/video/":"/home/dominik/mtk92/kernel/mediatek/kernel/drivers/sync/"
export C_INCLUDE_PATH

#Echo actual vars
echo "We are actually building for $TARGET_PRODUCT with $CROSS_COMPILE and platform path $MTK_PATH_PLATFORM"

#Workaround for + appended on kernelrelease
export LOCALVERSION=

#Create vars for OUT, SCRIPTS and RAMDISK directories
OUT_DIRECTORY=../out/$TARGET_PRODUCT
RAMDISK_DIRECTORY=ramdisk/$TARGET_PRODUCT
SCRIPTS_DIRECTORY=zip_scripts/$TARGET_PRODUCT
CERTIFICATES_DIRECTORY=../.certificates

#Create and clean out directory for your device
mkdir -p $OUT_DIRECTORY
if [ "$(ls -A $OUT_DIRECTORY)" ]; then
rm $OUT_DIRECTORY/* -R
fi

#Kernel part
#make O=out mediatek-configs
#make O=out -j all

make O=out g4s_defconfig
make O=out #-j4 MTK_GPL_PACKAGE=yes
cp out/arch/arm/boot/zImage $OUT_DIRECTORY/zImage
mtk-tools/mkimage $OUT_DIRECTORY/zImage KERNEL > $OUT_DIRECTORY/zImage_header

#Modules part
pushd out
make INSTALL_MOD_STRIP=--strip-unneeded INSTALL_MOD_PATH=$OUT_DIRECTORY/system INSTALL_MOD_DIR=$OUT_DIRECTORY/system android_modules_install
popd

#Repack part
if [ -d "$RAMDISK_DIRECTORY" ]; then
mtk-tools/repack-MT65xx.pl -boot $OUT_DIRECTORY/zImage_header $RAMDISK_DIRECTORY $OUT_DIRECTORY/boot.img
rm $OUT_DIRECTORY/zImage
rm $OUT_DIRECTORY/zImage_header
#Flashable zip build
if [ -d "$SCRIPTS_DIRECTORY" ]; then
cp $SCRIPTS_DIRECTORY/* $OUT_DIRECTORY -R
FLASHABLE_ZIP="$OUT_DIRECTORY/`cat DEVICE_NAME`"
FLASHABLE_ZIP_2="`cat DEVICE_NAME`"
echo "Creating flashable at '$FLASHABLE_ZIP'.zip"
pushd $OUT_DIRECTORY
zip -r -0 "$FLASHABLE_ZIP_2".zip .
popd
if [ ! -d "$CERTIFICATES_DIRECTORY" ]; then
echo "Warning ! We can't sign flashable.zip, you need to run ./certificates.sh"
else
java -jar $SCRIPTS_DIRECTORY/../signapk.jar $CERTIFICATES_DIRECTORY/certificate.pem $CERTIFICATES_DIRECTORY/key.pk8 "$FLASHABLE_ZIP".zip "$FLASHABLE_ZIP"-signed.zip
fi
fi
fi
